package ba.klika.mywallet.template.service;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.config.CustomRestExceptionHandler;
import ba.klika.mywallet.template.database.ClubsDatabaseService;
import ba.klika.mywallet.template.exception.ClubNotFoundException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
public class ClubsServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    IClubsService clubsService;

    @MockBean
    ClubsDatabaseService clubsDatabaseService;

    @Test
    public void createClubTest() throws Exception {
        when(clubsDatabaseService.createClub(any())).thenReturn(getClub());

        Club result = clubsService.createClub(getClubInput());

        assertThat(result.getName(), is("Arsenal"));
    }

    @Test
    public void updateClubTest() throws Exception {
        Long id = 1L;
        when(clubsDatabaseService.updateClub(eq(id), any())).thenReturn(getClub());

        Club result = clubsService.updateClub(id, getClubInput());

        assertThat(result.getName(), is("Arsenal"));
    }

    @Test
    public void getClubTest() throws Exception {
        Long id = 1L;
        when(clubsDatabaseService.getClubById(eq(id))).thenReturn(getClub());

        Club result = clubsService.getClub(id);

        assertThat(result.getName(), is("Arsenal"));
    }

    @Test(expected = ClubNotFoundException.class)
    public void getClubIncorrectIdTest() {
        Long id = 1L;
        when(clubsDatabaseService.getClubById(eq(id))).thenThrow(new ClubNotFoundException("Club not found."));

        clubsService.getClub(id);
    }
    private ClubInput getClubInput() {
        return new ClubInput().name("Arsenal");
    }

    private Club getClub() {
        Club club = new Club();
        club.setId(1L);
        club.setName("Arsenal");
        return club;
    }

    @TestConfiguration
    @Import(CustomRestExceptionHandler.class)
    static class ConfigForTest {
        @Bean
        public IClubsService getSut() { return new ClubsService(); }
    }
}
