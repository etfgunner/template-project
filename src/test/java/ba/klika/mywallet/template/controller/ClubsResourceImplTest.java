package ba.klika.mywallet.template.controller;

import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.controller.ClubsResourceImpl;
import ba.klika.mywallet.template.service.IClubsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(
		ClubsResourceImpl.class
)
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = {
		ClubsResourceImpl.class,
		IClubsService.class
})
public class ClubsResourceImplTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private IClubsService clubsService;

	@Test
	public void getClubs() throws Exception {
		ResultActions resultActions =
				mockMvc.perform(
						get("/clubs")
								.contentType(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON)
								.secure(false));

		resultActions.andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void getClub() throws Exception {
		String id = "1";

		ResultActions resultActions =
				mockMvc.perform(
						get("/clubs/" + id)
								.contentType(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON)
								.secure(false));

		resultActions.andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void createClub() throws Exception {
		ClubInput clubInput = getClubInput();
		ResultActions resultActions =
				mockMvc.perform(
						post("/clubs")
								.contentType(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(clubInput))
								.secure(false));

		resultActions.andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void updateClub() throws Exception {
		String id="1";
		ClubInput clubInput = getClubInput();
		ResultActions resultActions =
				mockMvc.perform(
						get("/clubs/" + id)
								.contentType(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(clubInput))
								.secure(false));

		resultActions.andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void deleteClub() throws Exception {
		String id = "1";
		ResultActions resultActions =
				mockMvc.perform(
						get("/clubs/" + id)
								.contentType(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON_VALUE)
								.accept(MediaType.APPLICATION_JSON)
								.secure(false));

		resultActions.andDo(print()).andExpect(status().isOk());
	}

	private ClubInput getClubInput() {
		return new ClubInput().name("Liverpool");
	}


}
