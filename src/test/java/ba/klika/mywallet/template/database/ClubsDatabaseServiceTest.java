package ba.klika.mywallet.template.database;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.config.CustomRestExceptionHandler;
import ba.klika.mywallet.template.database.dao.IClubsDao;
import ba.klika.mywallet.template.database.model.DBClub;
import ba.klika.mywallet.template.database.util.ClubMapper;
import ba.klika.mywallet.template.exception.ClubNotFoundException;
import ba.klika.mywallet.template.service.ClubsService;
import ba.klika.mywallet.template.service.IClubsService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ClubsDatabaseServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    ClubsDatabaseService clubsDatabaseService;

    @Autowired
    ClubMapper clubMapper;

    @MockBean
    IClubsDao clubsDao;

    @Test
    public void createClubTest() throws Exception {
        when(clubsDao.save(any())).thenReturn(getDBClub());

        Club result = clubsDatabaseService.createClub(getClubInput());

        assertThat(result.getName(), is("Arsenal"));
    }

    @Test
    public void updateClubTest() throws Exception {
        Long id = 1L;
        when(clubsDao.save(any())).thenReturn(getDBClub());
        when(clubsDao.findById(any())).thenReturn(Optional.of(getDBClub()));

        Club result = clubsDatabaseService.updateClub(id, getClubInput());

        assertThat(result.getName(), is("Arsenal"));
    }

    @Test
    public void getClubTest() throws Exception {
        Long id = 1L;
        when(clubsDao.findById(eq(id))).thenReturn(Optional.of(getDBClub()));

        Club result = clubsDatabaseService.getClubById(id);

        assertThat(result.getName(), is("Arsenal"));
    }

    @Test(expected = ClubNotFoundException.class)
    public void getClubIncorrectIdTest() {
        Long id = 1L;
        when(clubsDatabaseService.getClubById(eq(id))).thenThrow(new ClubNotFoundException("Club not found."));

        clubsDatabaseService.getClubById(id);
    }
    private ClubInput getClubInput() {
        return new ClubInput().name("Arsenal");
    }

    private DBClub getDBClub() {
        DBClub club = new DBClub();
        club.setId(1L);
        club.setName("Arsenal");
        return club;
    }

    @TestConfiguration
    @Import(CustomRestExceptionHandler.class)
    static class ConfigForTest {
        @Bean
        public ClubsDatabaseService getSut() { return new ClubsDatabaseService(); }

        @Bean
        public ClubMapper getClubMapper() { return new ClubMapper(); }
    }
}
