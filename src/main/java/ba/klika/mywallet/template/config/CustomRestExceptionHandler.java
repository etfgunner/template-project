package ba.klika.mywallet.template.config;

import ba.klika.mywallet.template.exception.ClubNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Object> handleAnyException(Exception exception) {

        // Add more mappings
        if (exception instanceof ClubNotFoundException) {
            return new ApiError(HttpStatus.NOT_FOUND, exception.getMessage()).toResponse();
        }

        return handleExceptionInternal(exception, null, null, null, null);
    }

}