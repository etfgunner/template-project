package ba.klika.mywallet.template.api;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClubList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-10-15T09:52:39.203Z")

public class ClubList   {
  @JsonProperty("clubs")
  @Valid
  private List<Club> clubs = null;

  public ClubList clubs(List<Club> clubs) {
    this.clubs = clubs;
    return this;
  }

  public ClubList addClubsItem(Club clubsItem) {
    if (this.clubs == null) {
      this.clubs = new ArrayList<Club>();
    }
    this.clubs.add(clubsItem);
    return this;
  }

  /**
   * Get clubs
   * @return clubs
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Club> getClubs() {
    return clubs;
  }

  public void setClubs(List<Club> clubs) {
    this.clubs = clubs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClubList clubList = (ClubList) o;
    return Objects.equals(this.clubs, clubList.clubs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(clubs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClubList {\n");
    
    sb.append("    clubs: ").append(toIndentedString(clubs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

