package ba.klika.mywallet.template.service;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.api.ClubList;
import ba.klika.mywallet.template.exception.ClubNotFoundException;


public interface IClubsService {

    ClubList getAllClubs();

    Club getClub(long id) throws ClubNotFoundException;

    Club createClub(ClubInput club);

    void deleteClub(long id);

    Club updateClub(long clubId, ClubInput club) throws ClubNotFoundException;
}
