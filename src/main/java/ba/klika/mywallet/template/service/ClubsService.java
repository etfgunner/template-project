package ba.klika.mywallet.template.service;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.api.ClubList;
import ba.klika.mywallet.template.database.ClubsDatabaseService;
import ba.klika.mywallet.template.exception.ClubNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClubsService implements IClubsService {

    @Autowired
    private ClubsDatabaseService clubsDatabaseService;

    @Override
    public ClubList getAllClubs() {
        return new ClubList()
                .clubs(clubsDatabaseService.getAllClubs());
    }

    @Override
    public Club getClub(long id) throws ClubNotFoundException {
        return clubsDatabaseService.getClubById(id);
    }

    @Override
    public Club createClub(ClubInput club) {
        return clubsDatabaseService.createClub(club);
    }

    @Override
    public void deleteClub(long id) {
        clubsDatabaseService.deleteClub(id);
    }

    @Override
    public Club updateClub(long clubId, ClubInput clubInput) throws ClubNotFoundException {
        return clubsDatabaseService.updateClub(clubId, clubInput);
    }
}
