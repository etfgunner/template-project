package ba.klika.mywallet.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class TemplateProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(TemplateProjectApplication.class, args);
	}
}
