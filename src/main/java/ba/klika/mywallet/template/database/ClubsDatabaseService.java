package ba.klika.mywallet.template.database;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.database.dao.IClubsDao;
import ba.klika.mywallet.template.database.model.DBClub;
import ba.klika.mywallet.template.database.util.ClubMapper;
import ba.klika.mywallet.template.exception.ClubNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClubsDatabaseService {

    @Autowired
    private IClubsDao clubsDao;

    @Autowired
    private ClubMapper clubMapper;

    public List<Club> getAllClubs() {
        return clubsDao.findAll()
                .stream()
                .map(c -> clubMapper.mapToApi(c))
                .collect(Collectors.toList());
    }

    public Club getClubById(long id) throws ClubNotFoundException {
        return clubsDao.findById(id)
                .map(c -> clubMapper.mapToApi(c))
                .orElseThrow(() -> new ClubNotFoundException("Club with given id not found."));
    }

    public Club createClub(ClubInput club) {
        DBClub dbClub = clubMapper.mapToDB(club);
        DBClub createdClub = clubsDao.save(dbClub);
        return clubMapper.mapToApi(createdClub);
    }

    public Club updateClub(long id, ClubInput club) throws ClubNotFoundException {
        DBClub retrievedClub = clubsDao.findById(id)
                .orElseThrow(() -> new ClubNotFoundException("Club with given id not found."));

        retrievedClub.setName(club.getName());
        clubsDao.save(retrievedClub);

        return clubMapper.mapToApi(retrievedClub);
    }

    public void deleteClub(long id) {

        try {
            clubsDao.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new ClubNotFoundException("Club with given id does not exist.");
        }
    }

}
