package ba.klika.mywallet.template.database.util;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.database.model.DBClub;
import org.springframework.stereotype.Component;

@Component
public class ClubMapper {

    public Club mapToApi(DBClub club){
        return new Club()
                    .id(club.getId())
                    .name(club.getName());
    }

    public DBClub mapToDB(ClubInput club){
        DBClub dbClub = new DBClub();
        dbClub.setName(club.getName());
        return dbClub;
    }
}
