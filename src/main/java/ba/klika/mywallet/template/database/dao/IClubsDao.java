package ba.klika.mywallet.template.database.dao;

import ba.klika.mywallet.template.database.model.DBClub;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IClubsDao extends JpaRepository<DBClub, Long> {
}