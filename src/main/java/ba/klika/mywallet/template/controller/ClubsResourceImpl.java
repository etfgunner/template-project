package ba.klika.mywallet.template.controller;

import ba.klika.mywallet.template.api.Club;
import ba.klika.mywallet.template.api.ClubInput;
import ba.klika.mywallet.template.api.ClubList;
import ba.klika.mywallet.template.api.ClubsApi;
import ba.klika.mywallet.template.exception.ClubNotFoundException;
import ba.klika.mywallet.template.service.IClubsService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ClubsResourceImpl implements ClubsApi {

	@Autowired
    IClubsService clubsService;

	@Override
    public ResponseEntity<Club> createClub(@Valid @RequestBody ClubInput body) {
	    return ResponseEntity.ok(clubsService.createClub(body));
    }

    @Override
    public ResponseEntity<Void> deleteClub(@PathVariable("clubId") Long clubId) {
	    clubsService.deleteClub(clubId);
	    return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Club> getClub(@PathVariable("clubId") Long clubId) {
	    return ResponseEntity.ok(clubsService.getClub(clubId));
    }

    @Override
    public ResponseEntity<ClubList> getClubs() {
	    return ResponseEntity.ok(clubsService.getAllClubs());
    }

    @Override
    public ResponseEntity<Club> updateClub(@PathVariable("clubId") Long clubId, @Valid @RequestBody ClubInput body) {
        return ResponseEntity.ok(clubsService.updateClub(clubId, body));
    }


}